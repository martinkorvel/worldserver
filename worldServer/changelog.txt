0.0.1 @date 21.03.2018
	Navigate through used commands in command input using up and down keys
	Added version number
	Click on version number shows change log
	Implemented "Clear" button
	Implemented "Stop Server" button
	Implemented port prompt on "Start server" click

0.0.2 @date 22.03.2018
	Port error is shown in log
	Version in window title
	Implemented blowfish cryptography
	Implemented "Clear log" button
	Log window font change
	Log window row colors
	Logging classes and types
	New server start dialog
	Configurable log levels
	Removed all warnings from code
	Added interfaces "iCommand" and "iResponse"
	
0.0.3 @date 23.03.2018
	Renamed "Gameserver" to "WorldServer"
	Added some error texts
	field "weigth" removed
	startServer(), stopServer() and resetLog() commands in console
	all console commands are now camel case
	implemented top menu
	new mysql error handling
	implemented restart server command
	implemented database view
	added help command
	added version command
	removed command buttons from GUI
	
0.0.4 @date XXX
MOVED TO BITBUCKET AND JIRA