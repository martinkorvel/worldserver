package org.nethost.data;

public class GameObjectSpawn {
	private int id;
	private int gameObject;
	private Vector3 position;
	private Quaternion rotation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGameObject() {
		return gameObject;
	}

	public void setGameObject(int gameObject) {
		this.gameObject = gameObject;
	}

	public Vector3 getPosition() {
		return position;
	}

	public void setPosition(Vector3 position) {
		this.position = position;
	}

	public Quaternion getRotation() {
		return rotation;
	}

	public void setRotation(Quaternion rotation) {
		this.rotation = rotation;
	}

}
