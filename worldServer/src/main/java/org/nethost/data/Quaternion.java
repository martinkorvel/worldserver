package org.nethost.data;

import org.nethost.util.Helpers;

public class Quaternion {
	private float x;
	private float y;
	private float z;
	private float w;

	public Quaternion() {

	}

	public Quaternion(float x, float y, float z, float w) {
		this.setX(Helpers.round(x, 2));
		this.setY(Helpers.round(y, 2));
		this.setZ(Helpers.round(z, 2));
		this.setZ(Helpers.round(w, 2));
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getW() {
		return w;
	}

	public void setW(float w) {
		this.w = w;
	}

	@Override
	public String toString() {
		return "Quaternion [x=" + x + ", y=" + y + ", z=" + z + ", w=" + w + "]";
	}

}
