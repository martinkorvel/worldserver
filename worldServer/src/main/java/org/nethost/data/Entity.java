package org.nethost.data;

public class Entity {
	private int id;
	private Item item;
	private Vector3 coordinates;
	private int count;
	private int chunk;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Vector3 getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Vector3 coordinates) {
		this.coordinates = coordinates;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getChunk() {
		return chunk;
	}

	public void setChunk(int chunk) {
		this.chunk = chunk;
	}

	@Override
	public String toString() {
		return "Entity [id=" + id + ", item=" + item + ", coordinates=" + coordinates + ", count=" + count + ", chunk="
				+ chunk + "]";
	}

}
