package org.nethost.data;

public class PlayerData {
	@Override
	public String toString() {
		return "PlayerData [uid=" + uid + ", position=" + position + ", rotation=" + rotation + "]";
	}

	private String uid;
	private Vector3 position;
	private Quaternion rotation;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Vector3 getPosition() {
		position.setY(position.getY() - 0.15f);
		return position;
	}

	public void setPosition(Vector3 position) {
		this.position = position;
	}

	public Quaternion getRotation() {
		return rotation;
	}

	public void setRotation(Quaternion rotation) {
		this.rotation = rotation;
	}
}
