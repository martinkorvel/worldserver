package org.nethost.data;

import org.nethost.util.Helpers;

public class Vector3 {
	private float x;
	private float y;
	private float z;

	public Vector3() {

	}

	public Vector3(float x, float y, float z) {
		this.setX(Helpers.round(x, 2));
		this.setY(Helpers.round(y, 2));
		this.setZ(Helpers.round(z, 2));
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "Vector3 [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
}
