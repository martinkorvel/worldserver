package org.nethost.handlers;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Base64;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.nethost.commands.CreateCharacterCommand;
import org.nethost.commands.CreateCharacterResponse;
import org.nethost.commands.GetCharacterCommand;
import org.nethost.commands.GetCharacterResponse;
import org.nethost.commands.GetCharactersCommand;
import org.nethost.commands.GetCharactersResponse;
import org.nethost.commands.GetItemCommand;
import org.nethost.commands.GetItemResponse;
import org.nethost.commands.GetItemsCommand;
import org.nethost.commands.GetItemsResponse;
import org.nethost.commands.GetObjectSpawnsCommand;
import org.nethost.commands.GetObjectSpawnsResponse;
import org.nethost.commands.SetPlayerDataCommand;
import org.nethost.commands.UserLoginCommand;
import org.nethost.commands.UserLoginResponse;
import org.nethost.globals.Variables;
import org.nethost.gui.MainGui;
import org.nethost.util.AdvancedEncryptionStandard;
import org.nethost.util.Gzip;
import org.quickserver.net.server.ClientCommandHandler;
import org.quickserver.net.server.ClientHandler;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class CommandHandler implements ClientCommandHandler {
	static final String RESPONSE_TO = "Response to ";

	public void handleCommand(ClientHandler handler, String command) {
		if (command.length() > 0) {
			try {
				if(Variables.getAESKey().length() > 0) {
					AdvancedEncryptionStandard aes = new AdvancedEncryptionStandard(Variables.getAESKey().getBytes(StandardCharsets.UTF_8));
					command = new String(aes.decrypt(Base64.getDecoder().decode(command.getBytes())));
				}
				command = Gzip.decompress(Base64.getDecoder().decode(command.getBytes()));
				MainGui.writeLogDebug("Command from client " + handler.getName());
				parseCommand(handler, command);
			} catch (Exception ex) {
				MainGui.writeLogError(ex.getMessage());

				try {
					sendClientMessage(handler, "ERR");
				} catch (Exception e) {
					MainGui.writeLogError(e.getMessage());
				}
			}
		}
	}

	public void parseCommand(ClientHandler handler, String command) throws SQLException {
		JSONObject obj = null;
		try {
			obj = (JSONObject) new JSONParser().parse(command);
		} catch (ParseException e) {
			MainGui.writeLogError("62 Unparsable command");

			sendClientMessage(handler, "ERR");
		}

		JSONObject jo = obj;
		
		if(jo == null) {
			throw new NullPointerException();
		}
		
		String inputCommand = (String) jo.get("command");
		inputCommand = inputCommand.toLowerCase();

		if (inputCommand.equals("getitem")) {
			JSONDeserializer<GetItemCommand> deserializer = new JSONDeserializer<>();
			GetItemCommand getItemCommand = deserializer.deserialize(command);
			
			if(!Variables.getAuthenticatedPlayers().containsKey(getItemCommand.getUid())) {
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.serialize(new GetItemResponse(getItemCommand));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("getitems")) {
			JSONDeserializer<GetItemsCommand> deserializer = new JSONDeserializer<>();
			GetItemsCommand getItemsCommand = deserializer.deserialize(command);
			
			if(!Variables.getAuthenticatedPlayers().containsKey(getItemsCommand.getUid())) {
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.include("items").serialize(new GetItemsResponse(getItemsCommand));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("getobjectspawns")) {
			JSONDeserializer<GetObjectSpawnsCommand> deserializer = new JSONDeserializer<>();
			GetObjectSpawnsCommand getObjectSpawnsCommand = deserializer.deserialize(command);
			
			MainGui.writeLogInfo(getObjectSpawnsCommand.getUid());
			
			if(!Variables.getAuthenticatedPlayers().containsKey(getObjectSpawnsCommand.getUid())) {				
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.include("gameObjectSpawns").serialize(new GetObjectSpawnsResponse(getObjectSpawnsCommand));

			MainGui.writeLogInfo(RESPONSE_TO + handler.getName());
			MainGui.writeLogInfo(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("userlogin")) {
			JSONDeserializer<UserLoginCommand> deserializer = new JSONDeserializer<>();
			UserLoginCommand userLoginCommand = deserializer.deserialize(command);

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.serialize(new UserLoginResponse(userLoginCommand, handler));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("createcharacter")) {
			JSONDeserializer<CreateCharacterCommand> deserializer = new JSONDeserializer<>();
			CreateCharacterCommand createCharacterCommand = deserializer.deserialize(command);
			
			if(!Variables.getAuthenticatedPlayers().containsKey(createCharacterCommand.getUid())) {
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.serialize(new CreateCharacterResponse(createCharacterCommand));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("getcharacter")) {
			JSONDeserializer<GetCharacterCommand> deserializer = new JSONDeserializer<>();
			GetCharacterCommand getCharacterCommand = deserializer.deserialize(command);
			
			if(!Variables.getAuthenticatedPlayers().containsKey(getCharacterCommand.getUid())) {
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.serialize(new GetCharacterResponse(getCharacterCommand));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("getcharacters")) {
			JSONDeserializer<GetCharactersCommand> deserializer = new JSONDeserializer<>();
			GetCharactersCommand getCharactersCommand = deserializer.deserialize(command);
			
			if(!Variables.getAuthenticatedPlayers().containsKey(getCharactersCommand.getUid())) {
				return;
			}

			JSONSerializer serializer = new JSONSerializer();
			String responseJson = serializer.include("characters").serialize(new GetCharactersResponse(getCharactersCommand));

			MainGui.writeLogDebug(RESPONSE_TO + handler.getName());
			MainGui.writeLogDebug(responseJson);

			sendClientMessage(handler, responseJson);
		} else if (inputCommand.equals("setplayerdata")) {
			JSONDeserializer<SetPlayerDataCommand> deserializer = new JSONDeserializer<>();
			SetPlayerDataCommand setPlayerDataCommand = deserializer.deserialize(command);

			if(!Variables.getAuthenticatedPlayers().containsKey(setPlayerDataCommand.getUid())) {
				return;
			}
			
			if(Variables.getPlayerData().containsKey(setPlayerDataCommand.getUid())) {	
				Variables.getPlayerData().replace(setPlayerDataCommand.getUid(), setPlayerDataCommand.getPlayerData());
			} else {
				Variables.getPlayerData().put(setPlayerDataCommand.getUid(), setPlayerDataCommand.getPlayerData());
			}
		}

	}

	public void sendClientMessage(ClientHandler handler, String data) {
		try {
			data = Base64.getEncoder().encodeToString(Gzip.compress(data));			
			
			//if we use crypto
			if(Variables.getAESKey().length() > 0) {
				AdvancedEncryptionStandard aes = new AdvancedEncryptionStandard(Variables.getAESKey().getBytes(StandardCharsets.UTF_8));
				data = Base64.getEncoder().encodeToString(aes.encrypt(data.getBytes(StandardCharsets.UTF_8)));
			}
			
			handler.sendClientMsg(data);
		} catch (Exception e) {
			MainGui.writeLogError(e.getMessage());
		}
	}
}