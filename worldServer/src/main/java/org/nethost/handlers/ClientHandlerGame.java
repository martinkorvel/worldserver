package org.nethost.handlers;

import java.net.*;
import java.io.*;

import org.nethost.globals.Variables;
import org.nethost.gui.MainGui;
import org.quickserver.net.server.ClientEventHandler;
import org.quickserver.net.server.ClientHandler;

public class ClientHandlerGame implements ClientEventHandler {
	
	static final String CLIENT = "Client ";

	public void gotConnected(ClientHandler handler) throws SocketTimeoutException {
		Variables.getConnectedClients().put(handler.getName(), handler);				
		MainGui.writeLogInfo(CLIENT + handler.getName() + " connected");
	}

	public void lostConnection(ClientHandler handler) throws IOException {		
		disconnectPlayer(handler.getName());
		MainGui.writeLogInfo(CLIENT + handler.getName() + " lost connection");
	}

	public void closingConnection(ClientHandler handler) throws IOException {
		disconnectPlayer(handler.getName());
		MainGui.writeLogInfo(CLIENT + handler.getName() + " disconnected");		
	}
	
	public void disconnectPlayer(String handlerName) {
		if(Variables.getClientNameToUID().containsKey(handlerName)) {
			if(Variables.getAuthenticatedPlayers().containsKey(Variables.getClientNameToUID().get(handlerName))) {
				Variables.getAuthenticatedPlayers().remove(Variables.getClientNameToUID().get(handlerName));
			}
			
			if(Variables.getPlayerData().containsKey(Variables.getClientNameToUID().get(handlerName))) {
				Variables.getPlayerData().remove(Variables.getClientNameToUID().get(handlerName));
			}
			
			Variables.getClientNameToUID().remove(handlerName);
		}
		
		Variables.getConnectedClients().remove(handlerName);
	}
}