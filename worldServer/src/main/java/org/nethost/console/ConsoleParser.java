package org.nethost.console;

import java.util.Map.Entry;

import org.nethost.commands.SendServerMessageResponse;
import org.nethost.database.Update;
import org.nethost.globals.Server;
import org.nethost.globals.Variables;
import org.nethost.gui.MainGui;
import org.quickserver.net.server.ClientHandler;

import flexjson.JSONSerializer;

public class ConsoleParser {
	private ConsoleParser() {
		
	}
	
	public static void parseCommand(String cmd) {
		if (cmd.trim().length() < 1) {
			return;
		}
		MainGui.writeLogConsole("Command: " + cmd);
		try {
			if (cmd.substring(0, cmd.indexOf('(')).equals("listClients")) {
				for (Entry<String, ClientHandler> entry : Variables.getConnectedClients().entrySet()) {
					MainGui.writeLogConsole(entry.getKey());
				}
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("kickClient")) {
				String parameters = cmd.substring(cmd.indexOf('(') + 1, cmd.indexOf(')'));
				String[] multipleParameters = parameters.split(",");

				String clientId = multipleParameters[0].replaceAll("\"", "");

				Variables.getConnectedClients().get(clientId).closeConnection();

				MainGui.writeLogConsole(clientId + " kicked");
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("sendServerMessage")) {
				String parameters = cmd.substring(cmd.indexOf('(') + 1, cmd.indexOf(')'));
				String[] multipleParameters = parameters.split(",");

				String message = multipleParameters[0].replaceAll("\"", "");

				ConsoleParser.sendServerMessage(message);

				MainGui.writeLogConsole("Sent server message to all clients");
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("startServer")) {
				Server.startServer();
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("stopServer")) {
				Server.stopServer();
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("restartServer")) {
				Server.restartServer();
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("resetLog")) {
				MainGui.resetLog();
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("help")) {
				getCommands();
			} else if (cmd.substring(0, cmd.indexOf('(')).equals("version")) {
				Update u = new Update();
				MainGui.writeLogConsole("Version " + Variables.getVersionMajor() + "." + Variables.getVersionMinor() + "." + Variables.getVersionBuild() + " Database version " + u.getDatabaseVersion());
			} else {
				MainGui.writeLogError("Command \"" + cmd + "\" not recognized!");
			}
		} catch (Exception ex) {
			MainGui.writeLogError("Command \"" + cmd + "\" not parseable!");			
		}
	}

	public static void getCommands() {
		StringBuilder sb = new StringBuilder();
	
		sb.append("listClients(), ");
		sb.append("kickClient(String clientId), ");
		sb.append("sendServerMessage(String message), ");
		sb.append("startServer(), ");
		sb.append("stopServer(), ");
		sb.append("restartServer(), ");
		sb.append("resetLog(), ");
		sb.append("help(), ");
		sb.append("version()");

		MainGui.writeLogConsole(sb.toString());
	}

	public static void sendServerMessage(String message) {
		JSONSerializer serializer = new JSONSerializer();
		String responseJson = serializer.include("items").serialize(new SendServerMessageResponse(message));

		MainGui.writeLogDebug("Server message");
		MainGui.writeLogDebug(responseJson);

		Server.broadcastToAllClients(responseJson, false);
	}
}
