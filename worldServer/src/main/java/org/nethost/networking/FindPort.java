package org.nethost.networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class FindPort {
	public int nextFreePort(int from, int to) {
		int port = randPort(from, to);
		
		while (true) {
			if (isLocalPortFree(port)) {
				return port;
			} else {
				port = ThreadLocalRandom.current().nextInt(from, to);
			}
		}
	}

	private int randPort(int from, int to) {
		Random r = new Random();
		return r.nextInt(to - from) + from;
	}

	private boolean isLocalPortFree(int port) {
		try {
			new ServerSocket(port).close();
			
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
