package org.nethost.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.nethost.globals.Server;
import org.nethost.globals.Variables;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;

public class StartSettings extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6072065172120763110L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtPort;
	private JTextField txtAESKey;

	/**
	 * Create the dialog.
	 */
	public StartSettings() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Variables.getMainGui().setEnabled(true);
				Variables.getMainGui().getMntmStartServer().setEnabled(false);
			}
		});

		setResizable(false);
		setAlwaysOnTop(true);
		setTitle("Startup settings");
		setBounds(100, 100, 233, 355);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		contentPanel.setLayout(null);

		txtPort = new JTextField();
		txtPort.setText("1000");
		txtPort.setBounds(102, 8, 115, 20);
		contentPanel.add(txtPort);
		txtPort.setColumns(10);

		JLabel lblPort = new JLabel("Port");
		lblPort.setLabelFor(txtPort);
		lblPort.setHorizontalAlignment(SwingConstants.LEFT);
		lblPort.setBounds(10, 11, 26, 14);
		contentPanel.add(lblPort);

		JCheckBox chckbxEnableInfoLog = new JCheckBox("Enable info log");
		chckbxEnableInfoLog.setSelected(true);
		chckbxEnableInfoLog.setBounds(6, 133, 107, 23);
		contentPanel.add(chckbxEnableInfoLog);

		JCheckBox chckbxEnableDebugLog = new JCheckBox("Enable debug log");
		chckbxEnableDebugLog.setBounds(6, 159, 132, 23);
		contentPanel.add(chckbxEnableDebugLog);

		JCheckBox chckbxEnableErrorLog = new JCheckBox("Enable error log");
		chckbxEnableErrorLog.setSelected(true);
		chckbxEnableErrorLog.setBounds(6, 185, 119, 23);
		contentPanel.add(chckbxEnableErrorLog);

		JCheckBox chckbxEnableConsoleLog = new JCheckBox("Enable console log");
		chckbxEnableConsoleLog.setSelected(true);
		chckbxEnableConsoleLog.setBounds(6, 211, 132, 23);
		contentPanel.add(chckbxEnableConsoleLog);

		JCheckBox chckbxEnableSuccessLog = new JCheckBox("Enable success log");
		chckbxEnableSuccessLog.setSelected(true);
		chckbxEnableSuccessLog.setBounds(6, 237, 143, 23);
		contentPanel.add(chckbxEnableSuccessLog);

		JCheckBox chckbxEnableRandomLog = new JCheckBox("Enable random log");
		chckbxEnableRandomLog.setSelected(true);
		chckbxEnableRandomLog.setBounds(6, 263, 132, 23);

		contentPanel.add(chckbxEnableRandomLog);

		JCheckBox chckbxUseCompression = new JCheckBox("Use compression");
		chckbxUseCompression.setSelected(true);
		chckbxUseCompression.setBounds(6, 85, 166, 23);
		contentPanel.add(chckbxUseCompression);

		JLabel lblAesKey = new JLabel("AES Key");
		lblAesKey.setBounds(10, 36, 82, 14);
		contentPanel.add(lblAesKey);

		txtAESKey = new JTextField();
		txtAESKey.setBounds(102, 33, 115, 20);
		contentPanel.add(txtAESKey);
		txtAESKey.setColumns(10);

		JLabel lblSettings = new JLabel("Settings");
		lblSettings.setBounds(10, 69, 207, 14);
		contentPanel.add(lblSettings);

		JLabel lblLoggingSettings = new JLabel("Logging settings");
		lblLoggingSettings.setBounds(10, 115, 207, 14);
		contentPanel.add(lblLoggingSettings);

		JLabel lblBytesKey = new JLabel("16 bytes key");
		lblBytesKey.setForeground(Color.RED);
		lblBytesKey.setBounds(103, 56, 114, 14);
		contentPanel.add(lblBytesKey);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton startButton = new JButton("Start");
		startButton.addActionListener(e -> {

			// aes key was entered
			if (txtAESKey.getText().length() > 0) {
				byte[] aesKeyBytes = txtAESKey.getText().getBytes();

				if (aesKeyBytes.length != 16) {
					String errMsg = "Invalid aes key: " + txtAESKey.getText();
					MainGui.writeLogError(errMsg);
					MainGui.setWarningMsg(errMsg);
					return;
				}
			}

			Variables.setEnableWriteLogInfo(chckbxEnableInfoLog.isSelected());
			Variables.setEnableWriteLogDebug(chckbxEnableDebugLog.isSelected());
			Variables.setEnableWriteLogError(chckbxEnableErrorLog.isSelected());
			Variables.setEnableWriteLogConsole(chckbxEnableConsoleLog.isSelected());
			Variables.setEnableWriteLogSuccess(chckbxEnableSuccessLog.isSelected());
			Variables.setEnableWriteLog(chckbxEnableRandomLog.isSelected());

			Variables.setAESKey(txtAESKey.getText());

			setVisible(false);
			Variables.getMainGui().setEnabled(true);

			int port = 0;

			String inputPort = txtPort.getText();

			try {
				port = Integer.parseInt(inputPort);
			} catch (Exception ex) {
				String errMsg = "Invalid port: " + inputPort;
				MainGui.writeLogError(errMsg);
				MainGui.setWarningMsg(errMsg);

				return;
			}

			if (chckbxUseCompression.isSelected()) {
				Variables.setUseCompression(true);
			} else {
				Variables.setUseCompression(false);
			}

			Variables.getMainGui().getMntmStartServer().setEnabled(false);
			Variables.getMainGui().enableButtonsOnServerStart();

			Server server = new Server();
			server.startServer(port);

			Variables.setServerRunning(true);

		});
		startButton.setActionCommand("OK");
		buttonPane.add(startButton);
		getRootPane().setDefaultButton(startButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(e -> {
			Variables.getMainGui().getMntmStartServer().setEnabled(true);
			Variables.getMainGui().setEnabled(true);
			setVisible(false);
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	}

}
