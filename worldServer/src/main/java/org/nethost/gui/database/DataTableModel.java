package org.nethost.gui.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

public class DataTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 7476614433411411902L;

	transient Object[][] tableData = null;
	LinkedHashMap<String, Object> columnNames = null;

	HashMap<Integer, ArrayList<Object>> data = new HashMap<>();

	public DataTableModel(Map<String, Object> columnNames, Map<Integer, ArrayList<Object>> data) {
		this.columnNames = (LinkedHashMap<String, Object>) columnNames;
		this.data = (HashMap<Integer, ArrayList<Object>>) data;

		initTable();
	}

	public void initTable() {
		tableData = new Object[data.keySet().size()][columnNames.size()];

		int index = 0;
		
		for (Entry<Integer, ArrayList<Object>> e : data.entrySet()) {
			int ix = 0;
			for (Object column : e.getValue()) {
				tableData[index][ix] = column;
				ix++;
			}
			index++;
		}
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	@Override
	public String getColumnName(int column) {
		Iterator<?> iterator = columnNames.entrySet().iterator();
		int n = 0;
		while (iterator.hasNext()) {
			@SuppressWarnings("unchecked")
			Entry<String, Object> entry = (Entry<String, Object>) iterator.next();
			if (n == column) {
				return entry.getKey();
			}
			n++;
		}
		return null;
	}

	@Override
	public int getRowCount() {
		return data.keySet().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return tableData[rowIndex][columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Iterator<?> iterator = columnNames.entrySet().iterator();
		int n = 0;
		while (iterator.hasNext()) {
			@SuppressWarnings("unchecked")
			Entry<String, Object> entry = (Entry<String, Object>) iterator.next();
			if (n == columnIndex && entry.getValue() instanceof Boolean) {
				return Boolean.class;
			}
			n++;
		}
		return super.getColumnClass(columnIndex);
	}

}