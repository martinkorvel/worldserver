package org.nethost.gui;

import javax.swing.JFrame;

import org.nethost.console.ConsoleParser;
import org.nethost.database.Database;
import org.nethost.globals.Server;
import org.nethost.globals.Variables;
import org.nethost.gui.database.DatabaseGui;
import org.nethost.util.Cache;
import org.nethost.util.ClassManager;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MainGui extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4309924126081022791L;

	private JTextField txtCommand;
	private static JTextPane txtAreaLog;
	private JButton btnSend;
	private JLabel lblVersion;

	private int lastUpKey = 0;
	private JButton btnClear;

	private JMenuBar jMenuBar;
	private JMenu mnFile;
	private JMenuItem mntmStartServer;
	private JMenuItem mntmStopServer;
	private JMenuItem mntmExit;
	private JMenu mnSystem;
	private JMenuItem mntmClearLog;
	private JMenuItem mntmReloadCache;
	private JMenu mnDatabase;
	private JMenuItem mntmItems;
	private JMenuItem mntmRestartServer;

	private static DatabaseGui dbGUI;

	protected static Map<String, Color> logRows = new LinkedHashMap<>();

	/**
	 * Create the panel.
	 */

	public MainGui() {
		if (Variables.isEditorMode()) {
			setTitle("World Server editor " + Variables.getVersionMajor() + "." + Variables.getVersionMinor() + "."
					+ Variables.getVersionBuild());
		} else {
			setTitle("World Server " + Variables.getVersionMajor() + "." + Variables.getVersionMinor() + "."
					+ Variables.getVersionBuild());
		}
		setResizable(false);
		getContentPane().setLayout(null);
		setBounds(200, 200, 600, 438);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		btnSend = new JButton("Send");
		btnSend.addActionListener(e -> {
			ConsoleParser.parseCommand(txtCommand.getText());
			Variables.getUsedCommands().add(txtCommand.getText());
			txtCommand.setText("");

			if (!Variables.getUsedCommands().isEmpty()) {
				lastUpKey = Variables.getUsedCommands().size() - 1;
			}
		});
		btnSend.setEnabled(false);
		btnSend.setBounds(505, 367, 79, 23);
		getContentPane().add(btnSend);

		txtCommand = new JTextField();
		txtCommand.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					ConsoleParser.parseCommand(txtCommand.getText());
					Variables.getUsedCommands().add(txtCommand.getText());
					txtCommand.setText("");

					if (!Variables.getUsedCommands().isEmpty()) {
						lastUpKey = Variables.getUsedCommands().size() - 1;
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					if (Variables.getUsedCommands().isEmpty()) {
						return;
					}
					if (lastUpKey < 0) {
						txtCommand.setText("");
						return;
					}
					String lastCommand = "";

					lastCommand = Variables.getUsedCommands().get(lastUpKey);
					txtCommand.setText(lastCommand);
					if (lastUpKey > 0) {
						lastUpKey--;
					}
				}

				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if (Variables.getUsedCommands().isEmpty()) {
						return;
					}
					if (lastUpKey >= Variables.getUsedCommands().size() - 1) {
						txtCommand.setText("");
						return;
					}

					String lastCommand = "";

					if (lastUpKey < Variables.getUsedCommands().size() - 1) {
						lastUpKey++;
					}

					lastCommand = Variables.getUsedCommands().get(lastUpKey);
					txtCommand.setText(lastCommand);
				}
			}
		});
		txtCommand.setEnabled(false);
		txtCommand.setBounds(10, 368, 390, 20);
		getContentPane().add(txtCommand);
		txtCommand.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 32, 574, 324);
		getContentPane().add(scrollPane);

		setTxtAreaLog(new JTextPane());
		scrollPane.setViewportView(txtAreaLog);
		txtAreaLog.setEditable(false);

		lblVersion = new JLabel("version");
		lblVersion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String msg = "<html>" + Variables.getVersionMajor() + "." + Variables.getVersionMinor() + "."
						+ Variables.getVersionBuild();

				JOptionPane optionPane = new JOptionPane();
				optionPane.setMessage(msg);
				optionPane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
				JDialog dialog = optionPane.createDialog(null, "Changelog");
				dialog.setVisible(true);
			}
		});
		lblVersion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVersion.setBounds(538, 393, 46, 14);
		lblVersion.setText(
				Variables.getVersionMajor() + "." + Variables.getVersionMinor() + "." + Variables.getVersionBuild());
		getContentPane().add(lblVersion);

		btnClear = new JButton("Clear");
		btnClear.setEnabled(false);
		btnClear.addActionListener(e -> txtCommand.setText(""));
		btnClear.setBounds(410, 367, 89, 23);
		getContentPane().add(btnClear);

		jMenuBar = new JMenuBar();
		jMenuBar.setBounds(0, 0, 594, 21);
		getContentPane().add(jMenuBar);

		mnFile = new JMenu("File");
		jMenuBar.add(mnFile);
		if (!Variables.isEditorMode()) {
			mntmStartServer = new JMenuItem("Start server");
			mntmStartServer.addActionListener(e -> Server.startServer());
			mnFile.add(mntmStartServer);

			mntmStopServer = new JMenuItem("Stop server");
			mntmStopServer.addActionListener(e -> Server.stopServer());
			mntmStopServer.setEnabled(false);
			mnFile.add(mntmStopServer);

			mntmRestartServer = new JMenuItem("Restart server");
			mntmRestartServer.addActionListener(e -> Server.restartServer());
			mntmRestartServer.setEnabled(false);
			mnFile.add(mntmRestartServer);

			mnFile.addSeparator();
		}
		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(e -> {
			if (Variables.isServerRunning()) {
				Server.stopServer();
			}

			System.exit(0);
		});
		mnFile.add(mntmExit);

		mnSystem = new JMenu("System");
		jMenuBar.add(mnSystem);

		mntmClearLog = new JMenuItem("Clear log");
		mntmClearLog.addActionListener(e -> MainGui.resetLog());
		mnSystem.add(mntmClearLog);

		if (!Variables.isEditorMode()) {
			mntmReloadCache = new JMenuItem("Reload cache");
			mntmReloadCache.addActionListener(e -> Cache.reloadCache());

			mntmReloadCache.setEnabled(false);

			mnSystem.add(mntmReloadCache);
		}

		mnDatabase = new JMenu("Database");
		jMenuBar.add(mnDatabase);

		try {
			for (Class<Database> cls : ClassManager.getClasses("org.nethost.database.objects")) {
				Database dbClass = cls.newInstance();

				mntmItems = new JMenuItem(dbClass.getTableName().replaceAll("_", " "));
				mntmItems.addActionListener(e -> {
					try {
						setDbGUI(new DatabaseGui(dbClass));
						dbGUI.setVisible(true);
					} catch (Exception ex) {
						MainGui.writeLogError(ex.getMessage());
					}
				});
				mnDatabase.add(mntmItems);

			}
		} catch (Exception e) {
			MainGui.writeLogError(e.toString());
		}

		Timer t = new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				// clone log hashmap
				HashMap<String, Color> clonedLogHashMap = (HashMap<String, Color>) logRows;
				setLogRows(new LinkedHashMap<>());
				if (clonedLogHashMap.size() > 0) {
					for (Entry<String, Color> e : clonedLogHashMap.entrySet()) {
						appendToPane(txtAreaLog, e.getKey(), e.getValue());
					}
				}
			}
		}, 0, 1000);

	}

	public void enableButtonsOnServerStart() {
		txtCommand.setEnabled(true);
		btnSend.setEnabled(true);
		btnClear.setEnabled(true);
	}

	// LOGGING FUNCTIONS
	public static void writeLogInfo(String log) {
		if (Variables.isEnableWriteLogInfo()) {
			MainGui.writeLog("INFO: " + log, Variables.getWriteLogInfoColor());
		}
	}

	public static void writeLogDebug(String log) {
		if (Variables.isEnableWriteLogDebug()) {
			MainGui.writeLog("DEBUG: " + log, Variables.getWriteLogDebugColor());
		}
	}

	public static void writeLogError(String log) {
		if (Variables.isEnableWriteLogError()) {
			MainGui.writeLog("ERROR: " + log, Variables.getWriteLogErrorColor());
		}
	}

	public static void writeLogConsole(String log) {
		if (Variables.isEnableWriteLogConsole()) {
			MainGui.writeLog("CONSOLE: " + log, Variables.getWriteLogConsoleColor());
		}
	}

	public static void writeLogSuccess(String log) {
		if (Variables.isEnableWriteLogSuccess()) {
			MainGui.writeLog("SUCCESS: " + log, Variables.getWriteLogSuccessColor());
		}
	}

	public static void writeLog(String log) {
		if (Variables.isEnableWriteLog()) {
			MainGui.writeLog("LOG: " + log, Variables.getWriteLogColor());
		}
	}

	public static void resetLog() {
		txtAreaLog.setText("");
	}

	// NO DIRECT ACCESS
	// Collects rows into map, and then renders then at fixed rate
	// every 0.1 sec
	private static void writeLog(String log, Color color) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String text = timestamp + ": " + log + "\r\n";

		logRows.put(text, color);
	}

	public static void appendToPane(JTextPane tp, String msg, Color c) {
		tp.setEditable(true);
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

		aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
		aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

		int len = tp.getDocument().getLength();
		tp.setCaretPosition(len);
		tp.setCharacterAttributes(aset, false);
		tp.replaceSelection(msg);
		tp.setEditable(false);
	}

	public static void setWarningMsg(String text) {
		Toolkit.getDefaultToolkit().beep();
		JOptionPane optionPane = new JOptionPane(text, JOptionPane.WARNING_MESSAGE);
		JDialog dialog = optionPane.createDialog("Warning!");
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}

	public JTextField getTxtCommand() {
		return txtCommand;
	}

	public void setTxtCommand(JTextField txtCommand) {
		this.txtCommand = txtCommand;
	}

	public static JTextPane getTxtAreaLog() {
		return txtAreaLog;
	}

	public static void setTxtAreaLog(JTextPane txtAreaLog) {
		MainGui.txtAreaLog = txtAreaLog;
	}

	public JButton getBtnSend() {
		return btnSend;
	}

	public void setBtnSend(JButton btnSend) {
		this.btnSend = btnSend;
	}

	public JLabel getLblVersion() {
		return lblVersion;
	}

	public void setLblVersion(JLabel lblVersion) {
		this.lblVersion = lblVersion;
	}

	public int getLastUpKey() {
		return lastUpKey;
	}

	public void setLastUpKey(int lastUpKey) {
		this.lastUpKey = lastUpKey;
	}

	public JButton getBtnClear() {
		return btnClear;
	}

	public void setBtnClear(JButton btnClear) {
		this.btnClear = btnClear;
	}

	public JMenuBar getjMenuBarMain() {
		return jMenuBar;
	}

	public void setjMenuBarMain(JMenuBar jMenuBar) {
		this.jMenuBar = jMenuBar;
	}

	public JMenu getMnFile() {
		return mnFile;
	}

	public void setMnFile(JMenu mnFile) {
		this.mnFile = mnFile;
	}

	public JMenuItem getMntmStartServer() {
		return mntmStartServer;
	}

	public void setMntmStartServer(JMenuItem mntmStartServer) {
		this.mntmStartServer = mntmStartServer;
	}

	public JMenuItem getMntmStopServer() {
		return mntmStopServer;
	}

	public void setMntmStopServer(JMenuItem mntmStopServer) {
		this.mntmStopServer = mntmStopServer;
	}

	public JMenuItem getMntmExit() {
		return mntmExit;
	}

	public void setMntmExit(JMenuItem mntmExit) {
		this.mntmExit = mntmExit;
	}

	public JMenu getMnSystem() {
		return mnSystem;
	}

	public void setMnSystem(JMenu mnSystem) {
		this.mnSystem = mnSystem;
	}

	public JMenuItem getMntmClearLog() {
		return mntmClearLog;
	}

	public void setMntmClearLog(JMenuItem mntmClearLog) {
		this.mntmClearLog = mntmClearLog;
	}

	public JMenuItem getMntmReloadCache() {
		return mntmReloadCache;
	}

	public void setMntmReloadCache(JMenuItem mntmReloadCache) {
		this.mntmReloadCache = mntmReloadCache;
	}

	public JMenu getMnDatabase() {
		return mnDatabase;
	}

	public void setMnDatabase(JMenu mnDatabase) {
		this.mnDatabase = mnDatabase;
	}

	public JMenuItem getMntmItems() {
		return mntmItems;
	}

	public void setMntmItems(JMenuItem mntmItems) {
		this.mntmItems = mntmItems;
	}

	public JMenuItem getMntmRestartServer() {
		return mntmRestartServer;
	}

	public void setMntmRestartServer(JMenuItem mntmRestartServer) {
		this.mntmRestartServer = mntmRestartServer;
	}

	public static DatabaseGui getDbGUI() {
		return dbGUI;
	}

	public static void setDbGUI(DatabaseGui dbGUI) {
		MainGui.dbGUI = dbGUI;
	}

	public static Map<String, Color> getLogRows() {
		return logRows;
	}

	public static void setLogRows(Map<String, Color> logRows) {
		MainGui.logRows = logRows;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
