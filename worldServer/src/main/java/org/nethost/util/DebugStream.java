package org.nethost.util;

import java.io.PrintStream;
import java.text.MessageFormat;

import org.nethost.gui.MainGui;

//overrides System.out.println() with custom method.
public class DebugStream extends PrintStream {
	private static final DebugStream INSTANCE = new DebugStream();

	public static void activate() {
		System.setOut(INSTANCE);
	}

	private DebugStream() {
		super(System.out);
	}

	@Override
	public void println(Object x) {
		String location = showLocation();
		super.println(x);
		MainGui.writeLogDebug(location + x.toString());
	}

	@Override
	public void println(String x) {
		String location = showLocation();
		super.println(x);
		MainGui.writeLogDebug(location + x);
	}

	private String showLocation() {
		StackTraceElement element = Thread.currentThread().getStackTrace()[3];
		super.print(MessageFormat.format("({0}:{1, number,#}) : ", element.getFileName(), element.getLineNumber()));

		return MessageFormat.format("({0}:{1, number,#}) : ", element.getFileName(), element.getLineNumber());
	}
}