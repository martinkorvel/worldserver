package org.nethost.util;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Helpers {
	private Helpers() {
		
	}
	//add slashes and replaces restricted characters from string. Can add some restrictions.
	public static String addSlashes(String s) {
		ArrayList<String> restrictedKeys = new ArrayList<>();
		restrictedKeys.add("current_timestamp");
		restrictedKeys.add("NOW()");
		
		if(restrictedKeys.contains(s)) {
			return s;
		}
		
	    s = s.replaceAll("\\\\", "\\\\\\\\");
	    s = s.replaceAll("\\n", "\\\\n");
	    s = s.replaceAll("\\r", "\\\\r");
	    s = s.replaceAll("\\00", "\\\\0");
	    s = s.replaceAll("'", "\\\\'");
	    return s;
	}
	
	//simple float rounding to given decimal places
	public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
