package org.nethost.commands;

import org.nethost.interfaces.Icommand;

public class CreateCharacterCommand implements Icommand {
	private String command;
	private String uid;
	private String name;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
