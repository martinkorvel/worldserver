package org.nethost.commands;

import org.nethost.interfaces.Icommand;

public class GetCharacterCommand implements Icommand{
	private String command;
	private String uid;
	private int id;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
