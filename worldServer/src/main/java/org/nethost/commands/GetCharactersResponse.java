package org.nethost.commands;

import java.util.ArrayList;
import java.util.List;

import org.nethost.data.GameCharacter;
import org.nethost.database.objects.GameCharacterDatabase;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Iresponse;

public class GetCharactersResponse implements Iresponse{
	private String command;
	private String result;
	private ArrayList<GameCharacter> characters = new ArrayList<>();
	
	public GetCharactersResponse(GetCharactersCommand cmd) {		
		command = cmd.getCommand();
		
		try {
			GameCharacterDatabase gcd = new GameCharacterDatabase();
			characters = (ArrayList<GameCharacter>) gcd.getCharacters(Variables.getAuthenticatedPlayers().get(cmd.getUid()));
			result = "OK";
		} catch (Exception ex){
			result = ex.getMessage();
			characters = new ArrayList<>();
		}	
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<GameCharacter> getCharacters() {
		return characters;
	}

	public void setCharacters(List<GameCharacter> characters) {
		this.characters = (ArrayList<GameCharacter>) characters;
	}

}
