package org.nethost.commands;

import java.sql.SQLException;
import java.util.UUID;

import org.nethost.database.objects.GameUser;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Iresponse;
import org.quickserver.net.server.ClientHandler;

public class UserLoginResponse implements Iresponse {
	private String command;
	private String result;
	private String uid;

	public UserLoginResponse(UserLoginCommand cmd, ClientHandler handler) throws SQLException {
		command = cmd.getCommand();
		
		GameUser gu = new GameUser();
		
		int userId = gu.doLogin(cmd.getUsername(), cmd.getPassword());
		
		if (userId > 0) {
			result = "OK";

			String uniqueID = UUID.randomUUID().toString();
			
			while (Variables.getAuthenticatedPlayers().containsKey(uniqueID)) {
				uniqueID = UUID.randomUUID().toString();
			}
			
			Variables.getClientNameToUID().put(handler.getName(), uniqueID);
			Variables.getAuthenticatedPlayers().put(uniqueID, userId);
			uid = uniqueID;
		} else {
			result = "ERR";
			uid = "";
		}
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
