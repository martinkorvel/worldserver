package org.nethost.commands;

import org.nethost.interfaces.Icommand;

public class GetObjectSpawnsCommand implements Icommand{
	private String command;
	private String uid;
	private int chunk;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getChunk() {
		return chunk;
	}

	public void setChunk(int chunk) {
		this.chunk = chunk;
	}
}
