package org.nethost.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.nethost.data.PlayerData;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Iresponse;

public class SendPlayerDataResponse implements Iresponse{
	private String command;
	private String result;	
	private ArrayList<PlayerData> playerData = new ArrayList<>();

	public SendPlayerDataResponse() {	
		command = "sendpositionsresponse";
		for (Entry<String, PlayerData> entry : Variables.getPlayerData().entrySet()) {			
			playerData.add(entry.getValue());
		}
		result = "OK";
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<PlayerData> getPlayerData() {
		return playerData;
	}

	public void setPlayerData(List<PlayerData> playerData) {
		this.playerData = (ArrayList<PlayerData>) playerData;
	}
}
