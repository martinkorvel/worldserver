package org.nethost.commands;

import org.nethost.interfaces.Icommand;

public class GetCharactersCommand implements Icommand{
	private String command;
	private String uid;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
