package org.nethost.commands;

import org.nethost.interfaces.Iresponse;

public class SendServerMessageResponse implements Iresponse{
	private String command = "sendservermessage";
	private String message;
	private String result;
	
	public SendServerMessageResponse(String message) {		
		this.message = message;
		result = "OK";
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
