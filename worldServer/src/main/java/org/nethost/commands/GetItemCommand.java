package org.nethost.commands;

import org.nethost.interfaces.Icommand;

public class GetItemCommand implements Icommand{
	private int id;
	private String command;
	private String uid;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
