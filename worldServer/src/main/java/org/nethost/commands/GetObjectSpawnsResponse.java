package org.nethost.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.nethost.data.GameObjectSpawn;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Iresponse;

public class GetObjectSpawnsResponse implements Iresponse{
	private String command;
	private String uid;
	private int chunk;
	private String result;
	private ArrayList<GameObjectSpawn> gameObjectSpawns = new ArrayList<>();

	public GetObjectSpawnsResponse(GetObjectSpawnsCommand cmd) {
		command = cmd.getCommand();
		uid = cmd.getUid();
		chunk = cmd.getChunk();
		result = "OK";

		for (Entry<Integer, GameObjectSpawn> entry : Variables.getGameObjectSpawnCache().entrySet()) {
			gameObjectSpawns.add(entry.getValue());
		}
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getChunk() {
		return chunk;
	}

	public void setChunk(int chunk) {
		this.chunk = chunk;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<GameObjectSpawn> getGameObjectSpawns() {
		return gameObjectSpawns;
	}

	public void setGameObjectSpawns(List<GameObjectSpawn> gameObjectSpawns) {
		this.gameObjectSpawns = (ArrayList<GameObjectSpawn>) gameObjectSpawns;
	}
}
