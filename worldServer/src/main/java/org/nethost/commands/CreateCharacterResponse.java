package org.nethost.commands;

import org.nethost.database.objects.GameCharacterDatabase;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Icommand;

public class CreateCharacterResponse implements Icommand {
	private String command;
	private String result;
	
	public CreateCharacterResponse(CreateCharacterCommand cmd) {		
		command = cmd.getCommand();
	
		try {
			GameCharacterDatabase gcd = new GameCharacterDatabase();
			gcd.insertCharacter(cmd.getName(), Variables.getAuthenticatedPlayers().get(cmd.getUid()));
			result = "OK";
		} catch (Exception ex){
			result = ex.getMessage();
		}	
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
