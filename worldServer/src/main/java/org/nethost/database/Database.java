package org.nethost.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nethost.gui.MainGui;
import org.nethost.util.Helpers;

import com.mysql.jdbc.Statement;

public class Database {
	public static final String WHERE = " WHERE ";
	public static final String FIELD = "Field";

	private String tableName;

	public String getTableName() {
		return tableName;
	}

	protected void setTableName(String tableName) {
		this.tableName = tableName;
	}

	protected void insert(String table, Map<String, Object> data) throws SQLException {
		StringBuilder qry = new StringBuilder();

		qry.append("INSERT INTO ");
		qry.append(table);

		StringBuilder keys = new StringBuilder();
		keys.append("(");
		StringBuilder values = new StringBuilder();
		values.append("(");

		boolean first = true;
		for (Entry<String, Object> row : data.entrySet()) {
			if (first) {
				keys.append(row.getKey());
				values.append("'" + Helpers.addSlashes(row.getValue().toString()) + "'");
			} else {
				keys.append(", " + row.getKey());
				values.append(", " + "'" + Helpers.addSlashes(row.getValue().toString()) + "'");
			}

			first = false;
		}

		keys.append(")");
		values.append(")");

		qry.append(keys);
		qry.append(" VALUES ");
		qry.append(values);

		pureSQL(qry.toString());
	}

	protected void update(String table, Map<String, Object> data) throws SQLException {
		update(table, data, "");
	}

	protected void update(String table, Map<String, Object> data, String where) throws SQLException {
		StringBuilder qry = new StringBuilder();

		qry.append("UPDATE ");
		qry.append(table);
		qry.append(" set ");

		for (Entry<String, Object> row : data.entrySet()) {
			qry.append(row.getKey() + "='" + Helpers.addSlashes(row.getValue().toString()) + "', ");
		}

		String noWhere = qry.toString();
		noWhere = noWhere.substring(0, noWhere.length() - 2);

		if (where.length() > 1) {
			noWhere = noWhere + WHERE + where;
		}

		pureSQL(noWhere);
	}

	protected List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> resultSet = new ArrayList<>();
		Statement st = null;
		ResultSet rs = null;

		try {
			st = (Statement) MysqlConnector.getConnection().createStatement();

			rs = st.executeQuery(sql);

			resultSet = resultSetToList(rs);
		} catch (Exception ex) {
			MainGui.writeLogError(ex.getMessage());
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				MainGui.writeLogError(e.getMessage());
			}
		}

		return resultSet;
	}

	protected void delete(String table, String where) throws SQLException {
		String qry = "DELETE FROM " + table + WHERE + where;
		pureSQL(qry);
	}

	protected void pureSQL(String sql) throws SQLException {
		Statement st = null;

		try {
			st = (Statement) MysqlConnector.getConnection().createStatement();
			st.execute(sql);
		} catch (Exception e) {
			MainGui.writeLogError(e.getMessage());
		} finally {
			if (st != null) {
				st.close();
			}
		}

	}

	protected boolean exists(String sql) throws SQLException {
		boolean result = false;

		try (Statement st = (Statement) MysqlConnector.getConnection().createStatement();
				ResultSet rs = st.executeQuery(sql)) {

			if (rs.next()) {
				result = true;
			}
		} catch (Exception e) {
			MainGui.writeLogError(e.getMessage());
		}
		return result;
	}

	public Map<String, Object> getColumNames() {
		Statement stmt = null;
		ResultSet rs = null;

		LinkedHashMap<String, Object> result = new LinkedHashMap<>();

		try {
			stmt = (Statement) MysqlConnector.getConnection().createStatement();

			if (stmt.execute("SHOW columns FROM " + getTableName())) {
				rs = stmt.getResultSet();
			}

			while (rs.next()) {
				String type = "";

				type = parseType(rs);

				if (type.equals("int")) {
					result.put(rs.getString(FIELD), 1);
				} else if (type.equals("tinyint")) {
					result.put(rs.getString(FIELD), true);
				} else {
					result.put(rs.getString(FIELD), "");
				}

			}

		} catch (SQLException ex) {
			MainGui.writeLogError(ex.getMessage());
			MainGui.writeLogError(ex.getSQLState());
			MainGui.writeLogError(String.valueOf(ex.getErrorCode()));
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				stmt = null;
			}

		}

		return result;
	}

	private String parseType(ResultSet rs) throws SQLException {
		try {
			return rs.getString("Type").substring(0, rs.getString("Type").indexOf('('));
		} catch (IndexOutOfBoundsException ex) {
			return rs.getString("Type");
		}
	}

	public Map<Integer, ArrayList<Object>> getTableData(String search) {
		Statement stmt = null;
		ResultSet rs = null;

		HashMap<Integer, ArrayList<Object>> items = new HashMap<>();
		HashMap<String, Object> colNames = (HashMap<String, Object>) getColumNames();

		try {
			stmt = (Statement) MysqlConnector.getConnection().createStatement();

			String qry = "";

			if (search == null) {
				qry = "SELECT * FROM " + getTableName() + " ORDER BY ID DESC";
			} else {
				StringBuilder like = new StringBuilder();
				for (Entry<String, Object> entry : colNames.entrySet()) {
					if (entry.getValue() instanceof String) {
						like.append(entry.getKey() + " LIKE '%" + search + "%' OR ");
					}
				}

				String likeResult = like.toString();

				likeResult = likeResult.substring(0, likeResult.length() - 3);

				qry = "SELECT * FROM " + getTableName() + WHERE + likeResult + " ORDER BY ID DESC";
			}

			if (stmt.execute(qry)) {
				rs = stmt.getResultSet();
			}

			int row = 0;

			if (rs != null) {
				while (rs.next()) {
					ArrayList<Object> item = new ArrayList<>();

					for (Entry<String, Object> entry : colNames.entrySet()) {
						if (entry.getValue() instanceof Integer) {
							item.add(rs.getInt(entry.getKey()));
						} else if (entry.getValue() instanceof Boolean) {
							item.add(rs.getBoolean(entry.getKey()));
						} else {
							item.add(rs.getString(entry.getKey()));
						}
					}

					items.put(row, item);
					row++;
				}
			}

		} catch (SQLException ex) {
			MainGui.writeLogError("SQLException: " + ex.getMessage());
			MainGui.writeLogError("SQLState: " + ex.getSQLState());
			MainGui.writeLogError("VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				stmt = null;
			}

		}

		return items;
	}

	public Map<Integer, ArrayList<Object>> getTableData() {
		return getTableData(null);
	}

	// updatemise loogika.
	protected static boolean tableExists(String tableName) {
		boolean exists = false;

		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			String qry = "show tables like ?";
			
			stmt =  MysqlConnector.getConnection().prepareStatement(qry);
			stmt.setString(1, tableName);			

			if (stmt.execute(qry)) {
				rs = stmt.getResultSet();
			}

			if (rs != null) {
				while (rs.next()) {
					exists = true;
				}
			}

		} catch (SQLException ex) {
			MainGui.writeLogError("SQLException: " + ex.getMessage());
			MainGui.writeLogError("SQLState: " + ex.getSQLState());
			MainGui.writeLogError("VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					MainGui.writeLogError(sqlEx.getMessage());
				}

				stmt = null;
			}

		}

		return exists;
	}

	private List<Map<String, Object>> resultSetToList(ResultSet rs) throws SQLException {
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		List<Map<String, Object>> rows = new ArrayList<>();
		while (rs.next()) {
			Map<String, Object> row = new HashMap<>(columns);
			for (int i = 1; i <= columns; ++i) {
				row.put(md.getColumnName(i), rs.getObject(i));
			}
			rows.add(row);
		}
		return rows;
	}
}
