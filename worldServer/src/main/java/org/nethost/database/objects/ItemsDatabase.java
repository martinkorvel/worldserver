package org.nethost.database.objects;

import java.util.List;
import java.util.Map;

import org.nethost.data.Item;
import org.nethost.database.Database;
import org.nethost.globals.Variables;

public class ItemsDatabase extends Database {

	public ItemsDatabase() {
		this.setTableName("game_item");
	}

	public Item getItem(int id) {

		if (Variables.getItemCache().containsKey(id)) {
			return Variables.getItemCache().get(id);
		} else {
			Item item = new Item();
			
			String sql = "SELECT * FROM " + this.getTableName() + " where id = " + id + " ORDER BY ID DESC";
			
			List<Map<String, Object>> result = select(sql);

			for (Map<String, Object> row : result) {
				item.setId((int) row.get("id"));
				item.setName((String) row.get("name"));
				item.setDescription((String) row.get("description"));
				item.setTradeable((boolean) Boolean.parseBoolean(String.valueOf("is_tradeable")));
				item.setStackable((boolean) Boolean.parseBoolean(String.valueOf("is_stackable")));
				item.setDropable((boolean) Boolean.parseBoolean(String.valueOf("is_dropable")));
				item.setWearable((boolean) Boolean.parseBoolean(String.valueOf("is_wearable")));

				item.setPrice((int) row.get("price"));
				item.setType((int) row.get("type"));

				Variables.getItemCache().put(item.getId(), item);
			}
			
			return item;
		}
	}

	public void fillItemCache() {
		String sql = "SELECT * FROM " + this.getTableName() + " ORDER BY ID DESC";
		
		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			Item item = new Item();
			
			item.setId((int) row.get("id"));
			item.setName((String) row.get("name"));
			item.setDescription((String) row.get("description"));
			item.setTradeable((boolean) Boolean.parseBoolean(String.valueOf("is_tradeable")));
			item.setStackable((boolean) Boolean.parseBoolean(String.valueOf("is_stackable")));
			item.setDropable((boolean) Boolean.parseBoolean(String.valueOf("is_dropable")));
			item.setWearable((boolean) Boolean.parseBoolean(String.valueOf("is_wearable")));

			item.setPrice((int) row.get("price"));
			item.setType((int) row.get("type"));

			Variables.getItemCache().put(item.getId(), item);
		}		
	}
}
