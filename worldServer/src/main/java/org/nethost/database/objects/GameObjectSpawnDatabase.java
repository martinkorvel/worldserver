package org.nethost.database.objects;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nethost.data.GameObject;
import org.nethost.data.GameObjectSpawn;
import org.nethost.data.Quaternion;
import org.nethost.data.Vector3;
import org.nethost.database.Database;
import org.nethost.globals.Variables;

public class GameObjectSpawnDatabase extends Database {
	public static final String GAMEOBJECT = "game_object";
	public static final String POSITION_X = "position_x";
	public static final String POSITION_Y = "position_y";
	public static final String POSITION_Z = "position_z";
	public static final String ROTATION_X = "rotation_x";
	public static final String ROTATION_Y = "rotation_y";
	public static final String ROTATION_Z = "rotation_z";
	public static final String ROTATION_W = "rotation_w";
	
	public GameObjectSpawnDatabase() {
		this.setTableName("game_object_spawn");
	}

	public GameObjectSpawn getGameObjectSpawn(int id) {

		if (Variables.getGameObjectSpawnCache().containsKey(id)) {
			return Variables.getGameObjectSpawnCache().get(id);
		} else {
			GameObjectSpawn gameObjectSpawn = new GameObjectSpawn();

			String sql = "SELECT * FROM " + this.getTableName() + " where id = " + id;

			List<Map<String, Object>> result = select(sql);

			for (Map<String, Object> row : result) {
				gameObjectSpawn.setId((int) row.get("id"));
				gameObjectSpawn.setGameObject((int) row.get(GAMEOBJECT));
				gameObjectSpawn.setPosition(new Vector3((float) row.get(POSITION_X), (float) row.get(POSITION_Y),
						(float) row.get(POSITION_Z)));
				gameObjectSpawn.setRotation(new Quaternion((float) row.get(ROTATION_X), (float) row.get(ROTATION_Y),
						(float) row.get(ROTATION_Z), (float) row.get(ROTATION_W)));

				Variables.getGameObjectSpawnCache().put(gameObjectSpawn.getId(), gameObjectSpawn);
			}

			return gameObjectSpawn;
		}
	}

	public void fillGameObjectSpawnCache() {
		String sql = "SELECT * FROM " + this.getTableName() + " ORDER BY ID DESC";

		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			GameObjectSpawn gameObjectSpawn = new GameObjectSpawn();

			gameObjectSpawn.setId((int) row.get("id"));
			gameObjectSpawn.setGameObject((int) row.get(GAMEOBJECT));
			gameObjectSpawn.setPosition(new Vector3((float) row.get(POSITION_X), (float) row.get(POSITION_Y),
					(float) row.get(POSITION_Z)));
			gameObjectSpawn.setRotation(new Quaternion((float) row.get(ROTATION_X), (float) row.get(ROTATION_Y),
					(float) row.get(ROTATION_Z), (float) row.get(ROTATION_W)));

			Variables.getGameObjectSpawnCache().put(gameObjectSpawn.getId(), gameObjectSpawn);
		}
	}

	public void insertGameObjectSpawn(GameObject go, float posX, float posY, float posZ, float rotX, float rotY,
			float rotZ, float rotW) throws SQLException {

		HashMap<String, Object> values = new HashMap<>();

		values.put(GAMEOBJECT, go.getId());
		values.put(POSITION_X, posX);
		values.put(POSITION_Y, posY);
		values.put(POSITION_Z, posZ);
		values.put(ROTATION_X, rotX);
		values.put(ROTATION_Y, rotY);
		values.put(ROTATION_Z, rotZ);
		values.put(ROTATION_W, rotW);

		insert(this.getTableName(), values);

	}	
}
