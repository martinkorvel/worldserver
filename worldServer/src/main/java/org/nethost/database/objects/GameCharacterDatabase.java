package org.nethost.database.objects;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nethost.data.GameCharacter;
import org.nethost.database.Database;
import org.nethost.exception.InvalidInputException;
import org.nethost.util.Helpers;

public class GameCharacterDatabase extends Database {
	public static final String USER_ID = "user_id";	

	public GameCharacterDatabase() {
		this.setTableName("game_user_character");
	}

	public List<GameCharacter> getCharacters(int userId) {
		ArrayList<GameCharacter> agc = new ArrayList<>();

		String sql = "SELECT * FROM " + this.getTableName() + " WHERE user_id = " + userId + " ORDER BY ID DESC";

		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			GameCharacter gc = new GameCharacter();

			gc.setId((int) row.get("id"));
			gc.setUserId((int) row.get(USER_ID));
			gc.setExp((int) row.get("exp"));
			gc.setName((String) row.get("name"));

			agc.add(gc);
		}

		return agc;
	}

	public GameCharacter getCharacter(int id) {
		GameCharacter gc = new GameCharacter();

		String sql = "SELECT * FROM " + this.getTableName() + " WHERE id = " + id;

		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			gc.setId((int) row.get("id"));
			gc.setUserId((int) row.get(USER_ID));
			gc.setExp((int) row.get("exp"));
			gc.setName((String) row.get("name"));
		}

		return gc;
	}

	public void insertCharacter(String name, int userId) throws InvalidInputException, SQLException {
		String sql = "";

		name = name.trim();

		if (name.length() < 2) {
			throw new InvalidInputException("Name cannot be empty!");
		}

		name = Helpers.addSlashes("name");

		sql = "SELECT 1 FROM " + this.getTableName() + " WHERE name='" + name + "'";

		if (exists(sql)) {
			throw new InvalidInputException("A character with that name already exists!");
		}

		HashMap<String, Object> values = new HashMap<>();
		values.put("name", name);
		values.put(USER_ID, userId);

		insert(this.getTableName(), values);
	}
}
