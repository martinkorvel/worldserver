package org.nethost.database.objects;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nethost.data.GameObject;
import org.nethost.database.Database;
import org.nethost.exception.InvalidInputException;
import org.nethost.globals.Variables;

public class GameObjectDatabase extends Database {

	public GameObjectDatabase() {
		this.setTableName("game_object");
	}

	public GameObject getGameObject(int id) {
		if (Variables.getGameObjectCache().containsKey(id)) {
			return Variables.getGameObjectCache().get(id);
		} else {
			ArrayList<GameObject> list = getGameObjects(id);

			if (!list.isEmpty()) {
				return list.get(0);
			}
		}

		return new GameObject();
	}

	public void insertGameObject(String name, String description, boolean interactable, int type) throws InvalidInputException, SQLException {
		name = name.trim();
		description = description.trim();
		int interactableInt = 0;

		if (interactable) {
			interactableInt = 1;
		}

		if (name.length() < 2) {
			throw new InvalidInputException("Name cannot be empty!");
		}

		if (description.length() < 2) {
			throw new InvalidInputException("Description cannot be empty!");
		}

		HashMap<String, Object> values = new HashMap<>();

		values.put("name", name);
		values.put("description", description);
		values.put("interactable", interactableInt);
		values.put("type", type);

		insert(this.getTableName(), values);

	}

	public void fillGameObjectCache() {
		ArrayList<GameObject> results = (ArrayList<GameObject>) getGameObjects();

		for (GameObject gameObject : results) {
			Variables.getGameObjectCache().put(gameObject.getId(), gameObject);
		}
	}

	public List<GameObject> getGameObjects() {
		return getGameObjects(0);
	}

	private ArrayList<GameObject> getGameObjects(int id) {
		ArrayList<GameObject> gameObjectList = new ArrayList<>();

		String sql = "";

		if (id > 0) {
			sql = "SELECT * FROM " + this.getTableName() + " WHERE id = " + id + " ORDER BY ID DESC";
		} else {
			sql = "SELECT * FROM " + this.getTableName() + " ORDER BY ID DESC";
		}

		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			GameObject gameObject = new GameObject();

			gameObject.setId((int) row.get("id"));
			gameObject.setName((String) row.get("name"));
			gameObject.setDescription((String) row.get("description"));
			gameObject.setInteractable((boolean) Boolean.parseBoolean(String.valueOf(row.get("interactable"))));
			gameObject.setType((int) row.get("type"));

			gameObjectList.add(gameObject);
		}

		return gameObjectList;
	}
}
