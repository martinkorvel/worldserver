package org.nethost.database.objects;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nethost.database.Database;
import org.nethost.exception.InvalidInputException;

public class GameUser extends Database {
	public GameUser() {
		this.setTableName("game_user");
	}
	
	public void insertUser(String username, String password) throws SQLException, InvalidInputException {
		if(exists("SELECT 1 FROM " + this.getTableName() + " WHERE username='" + username + "'")) {
			throw new InvalidInputException("Username exists!");
		}
		
		HashMap<String, Object> ins = new HashMap<>();
		ins.put("username", username);
		ins.put("password", password);
		
		insert(this.getTableName(), ins);
	}
	
	public int doLogin(String username, String password) throws SQLException {
		int uid = 0;
			
		String sql = "SELECT id FROM " + this.getTableName() + " WHERE username='"+username+"' and password='"+password+"'";
		
		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			uid = (int) row.get("id");
		}
		
		if(uid > 0) {
			HashMap<String, Object> values = new HashMap<>();
			values.put("last_login", "current_timestamp");
			update(this.getTableName(), values);
		}
		
		return uid;
	}
}
