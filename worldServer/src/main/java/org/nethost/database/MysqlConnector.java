package org.nethost.database;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.nethost.gui.MainGui;

import com.mysql.jdbc.Connection;

public class MysqlConnector {
	private MysqlConnector() {

	}

	private static Connection conn = null;

	public static Connection getConnection() {
		try {
			if (conn == null || conn.isClosed()) {
				conn = (Connection) DriverManager.getConnection(
						"jdbc:mysql://localhost/game_dev?" + "user=game_dev&password=GameDev&useSSL=false");
			}
		} catch (SQLException e) {
			MainGui.writeLogError(e.getMessage());
		}

		return conn;
	}
}
