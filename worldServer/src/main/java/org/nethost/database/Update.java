package org.nethost.database;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Update extends Database {
	public static final String VERSIONSTRING = "version";
	private Map<Integer, String> updates = new HashMap<>();

	public Update() {
		updates.put(1,
				"create table version(id integer not null auto_increment, version_nr integer, update_time timestamp not null default current_timestamp, primary key(id))");
		updates.put(2,
				"create table game_item (id integer not null auto_increment, name text, description longtext, is_tradeable tinyint, is_stackable tinyint, is_dropable tinyint, is_wearable tinyint, price integer, type integer, primary key(id))");
		updates.put(3,
				"CREATE TABLE game_object (id INTEGER NOT NULL AUTO_INCREMENT, name TEXT, description LONGTEXT, interactable TINYINT, type INTEGER, PRIMARY KEY(id))");
		updates.put(4, "ALTER TABLE game_object ADD game_object_name TEXT");
		updates.put(5, "SELECT 1");
		updates.put(6,
				"CREATE TABLE game_object_spawn(id INTEGER NOT NULL AUTO_INCREMENT, game_object INTEGER, position_x FLOAT, position_y FLOAT, position_z FLOAT, rotation_x FLOAT, rotation_y FLOAT, rotation_z FLOAT, PRIMARY KEY(id))");
		updates.put(7, "ALTER TABLE game_object_spawn add rotation_w float");
		updates.put(8, "ALTER TABLE game_object DROP game_object_name");
		updates.put(9,
				"CREATE TABLE game_user(id INTEGER NOT NULL AUTO_INCREMENT, username text, password text, created timestamp not null default current_timestamp, last_login datetime, primary key(id))");
		updates.put(10,
				"CREATE TABLE game_user_character(id integer not null auto_increment, user_id integer, name text, exp bigint, primary key(id))");
		updates.put(11,
				"ALTER TABLE game_user_character ADD CONSTRAINT fk_user_character_2_user FOREIGN KEY (user_id) REFERENCES game_user(id) ON DELETE CASCADE");
		updates.put(12, "ALTER TABLE game_user_character CHANGE COLUMN exp exp BIGINT(20) DEFAULT 0");
		updates.put(13,
				"ALTER TABLE game_object_spawn ADD CONSTRAINT fk_game_object_spawn_2_game_object FOREIGN KEY (game_object) REFERENCES game_object(id) ON DELETE CASCADE");
	}

	public void updateDatabase() throws SQLException {
		for (Entry<Integer, String> entry : updates.entrySet()) {
			if (entry.getKey() > getDatabaseVersion()) {
				pureSQL(entry.getValue());

				HashMap<String, Object> values = new HashMap<>();
				values.put("version_nr", entry.getKey());
				insert(VERSIONSTRING, values);
			}
		}
	}

	public int getDatabaseVersion() {
		int version = 0;

		boolean versionExists = Database.tableExists(VERSIONSTRING);

		if (!versionExists) {
			return 0;
		}

		String sql = "SELECT max(version_nr) as version FROM version";

		List<Map<String, Object>> result = select(sql);

		for (Map<String, Object> row : result) {
			version = (int) row.get(VERSIONSTRING);
		}

		return version;
	}
}
