package org.nethost.database;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class SqlCommands extends Database{
	public void sqlInsert(String table, Map<String, Object> data) throws SQLException {
		insert(table, data);
	}
	public void sqlUpdate(String table, Map<String, Object> data, String where) throws SQLException {
		update(table, data, where);
	}
	public void sqlDelete(String table, String where) throws SQLException {
		delete(table, where);
	}
	public boolean sqlExists(String sql) throws SQLException {
		return exists(sql);
	}
	
	public List<Map<String, Object>> sqlSelect(String sql) {
		return select(sql);
	}
}
