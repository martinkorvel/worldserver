package org.nethost.globals;

import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JDialog;

import org.apache.commons.codec.binary.Base64;
import org.nethost.console.ConsoleParser;
import org.nethost.database.Update;
import org.nethost.gui.MainGui;
import org.nethost.gui.StartSettings;
import org.nethost.handlers.GlobalsSender;
import org.nethost.util.AdvancedEncryptionStandard;
import org.nethost.util.Cache;
import org.nethost.util.Gzip;
import org.quickserver.net.AppException;
import org.quickserver.net.server.ClientHandler;
import org.quickserver.net.server.QuickServer;

public class Server {
	public static final String BIND_IP = "0.0.0.0";

	public void startServer(int port) {
		MainGui.resetLog();
		// initialize connectedClients HashMap
		MainGui.writeLogInfo("Initializing connectedClients HashMap...");
		Variables.setConnectedClients(new HashMap<String, ClientHandler>());
		MainGui.writeLogSuccess("Done initializing connectedClients HashMap");

		MainGui.writeLogInfo("Initializing QuickServer...");
		QuickServer qs = new QuickServer();
		MainGui.writeLogSuccess("Done initializing QuickServer");

		try {
			qs.setBindAddr(BIND_IP);
		} catch (UnknownHostException e) {
			MainGui.writeLogError(e.getMessage());
		}
		qs.setPort(port);
		qs.setName("WorldServer");
		qs.setClientEventHandler("org.nethost.handlers.ClientHandlerGame");
		qs.setClientCommandHandler("org.nethost.handlers.CommandHandler");
		qs.setTimeout(3600000);

		// write log
		MainGui.writeLogInfo("Server port: " + port);
		MainGui.writeLogInfo("Server name: " + qs.getName());
		MainGui.writeLogInfo("Server clientEventHandler: " + qs.getClientEventHandler());
		MainGui.writeLogInfo("Server clientCommandHandler: " + qs.getClientCommandHandler());
		MainGui.writeLogInfo("Server timeout: " + qs.getTimeout());

		// init console
		MainGui.writeLogInfo("Console initialized!");
		MainGui.writeLogInfo("Availible commands");
		ConsoleParser.getCommands();

		if (Variables.isUseCompression()) {
			MainGui.writeLogInfo("Using Gzip compression on packets");
		}

		if (Variables.getAESKey().length() > 0) {
			MainGui.writeLogInfo("Using AES encryption on packets. Using key ".concat(Variables.getAESKey()));
		}

		try {
			qs.startServer();
			Variables.setQs(qs);
			MainGui.writeLogSuccess("SERVER STARTED!");
			Variables.getMainGui().getMntmReloadCache().setEnabled(true);

			// init globals
			GlobalsSender.sendPlayerData(100);
		} catch (AppException ex) {
			MainGui.writeLogError(ex.getMessage());
		}
	}

	public void startMySql() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			MainGui.writeLogSuccess("Loaded com.mysql.jdbc.Driver");
			MainGui.writeLogInfo("Starting updater...");

			Update update = new Update();
			update.updateDatabase();

			MainGui.writeLogSuccess("Updater done. Database version " + update.getDatabaseVersion());
		} catch (Exception ex) {
			MainGui.writeLogError(ex.getMessage());
		}
	}

	public static void broadcastToAllClients(String data, boolean onlyAuthed) {
		try {
			data = Base64.encodeBase64String(Gzip.compress(data));

			// if we use crypto
			if (Variables.getAESKey().length() > 0) {
				AdvancedEncryptionStandard aes = new AdvancedEncryptionStandard(
						Variables.getAESKey().getBytes(StandardCharsets.UTF_8));
				data = Base64.encodeBase64String(aes.encrypt(data.getBytes(StandardCharsets.UTF_8)));
			}
		} catch (Exception e1) {
			MainGui.writeLogError("Cannot gzip! " + e1.getMessage());
		}

		for (Entry<String, ClientHandler> entry : Variables.getConnectedClients().entrySet()) {
			if (onlyAuthed && !Variables.getClientNameToUID().containsKey(entry.getKey())) {
				continue;
			}
			try {
				entry.getValue().sendClientMsg(data);
			} catch (Exception e) {
				MainGui.writeLogError(e.getMessage());
			}
		}

	}

	public static void broadcastToAllClientsExcept(String data, String name, boolean onlyAuthed) {
		try {
			data = Base64.encodeBase64String(Gzip.compress(data));

			// if we use crypto
			if (Variables.getAESKey().length() > 0) {
				AdvancedEncryptionStandard aes = new AdvancedEncryptionStandard(
						Variables.getAESKey().getBytes(StandardCharsets.UTF_8));
				data = Base64.encodeBase64String(aes.encrypt(data.getBytes(StandardCharsets.UTF_8)));
			}
		} catch (Exception e1) {
			MainGui.writeLogError("Cannot gzip! " + e1.getMessage());
		}

		for (Entry<String, ClientHandler> entry : Variables.getConnectedClients().entrySet()) {
			if (entry.getKey().equals(name) || (onlyAuthed && !Variables.getClientNameToUID().containsKey(entry.getKey()))) {
				continue;
			}

			try {
				entry.getValue().sendClientMsg(data);
			} catch (Exception e) {
				MainGui.writeLogError(e.getMessage());
			}
		}
	}

	public static void startServer() {
		StartSettings dialog = new StartSettings();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);

		Variables.getMainGui().getMntmStartServer().setEnabled(false);
		Variables.getMainGui().getMntmStopServer().setEnabled(true);
		Variables.getMainGui().getMntmRestartServer().setEnabled(true);
		Variables.getMainGui().setEnabled(false);
	}

	public static void stopServer() {
		try {
			Variables.getQs().stopServer();
			Variables.setConnectedClients(null);
			Cache.clearCache();
			Variables.setQs(null);

			MainGui.writeLogSuccess("SERVER STOPPED!");

			Variables.getMainGui().getMntmStartServer().setEnabled(true);

			Variables.getMainGui().getTxtCommand().setEnabled(false);
			Variables.getMainGui().getBtnSend().setEnabled(false);
			Variables.getMainGui().getBtnClear().setEnabled(false);
			Variables.getMainGui().getMntmStopServer().setEnabled(false);
			Variables.getMainGui().getMntmRestartServer().setEnabled(false);
			Variables.getMainGui().getMntmReloadCache().setEnabled(false);

			Variables.setServerRunning(false);
		} catch (Exception ex) {
			MainGui.writeLogError(ex.getMessage());
		}
	}

	public static void restartServer() {
		stopServer();
		startServer();
	}
}
