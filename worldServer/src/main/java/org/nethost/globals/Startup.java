package org.nethost.globals;

import javax.swing.SwingUtilities;

import org.nethost.gui.MainGui;
import org.nethost.util.Cache;
import org.nethost.util.DebugStream;

public class Startup {
	public static void main(String[] args) {		
		DebugStream.activate();
		Variables.setEditorMode(false);
		MainGui gui = new MainGui();
		
		SwingUtilities.invokeLater(() -> gui.setVisible(true));
		
		Variables.setMainGui(gui);

		Server s = new Server();
		s.startMySql();
		
		Cache.reloadCache();
	}

}
